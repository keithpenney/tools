import json
import numpy
import math

# "fclk100":{"access": "r", "addr_width": 0, "data_width": 32, "sign": "unsigned", "init": "0", "base_addr": 1}

# TODO:
#   Add support for "keep" base_addr's which specify a register should go exactly in that spot, raising an exception if it doesn't fit
#   Add support for "volatile" attribute which forces an "rw" register to go to the SCALAR region
#
#  Segmented Memory Map
#  Memory range divided into regions of different behavior.
#  Registers are assigned to regions based on the characteristics of the way they are accessed.
#  aw  r/w   region  treatment
#  ---------------------------
#  0   r     SCALAR  Pipelined mux
#  0   rw    MIRROR  Pipelined write to both register and mirror array, read from mirror array
#  >0  --    ARRAY   Read directly from array

class Args():
    wgate_continuous = False
    wstb_continuous = False
    wen_continuous = False
    latch_on_strobe = True
    rstb_continuous = True
    ren_continuous = True
    stb_from_en = True
    wdata_on_wen = False
    use_mirror = False
    scalar_pipeline_depth = 0
    #bus_latched = True  # When true, latch waddr, wren, wdata

    def __init__(self, **kwargs):
        for kw,val in kwargs.items():
            if hasattr(self, kw):
                setattr(self, kw, val)

# Gang's original configuration
conf_0 = Args(
    wgate_continuous=False,
    wstb_continuous=False,
    wen_continuous=False,
    latch_on_strobe=True,
    rstb_continuous=False,
    ren_continuous=False,
    stb_from_en=True,
    wdata_on_wen=True,
    use_mirror=False,
    scalar_pipeline_depth=0
)

# Test configuration 1: Reduced cycles
conf_1 = Args(
    wgate_continuous=True,
    wstb_continuous=True,
    wen_continuous=True,
    latch_on_strobe=True,
    rstb_continuous=True,
    ren_continuous=True,
    stb_from_en=False,
    wdata_on_wen=False,
    use_mirror=True,
    scalar_pipeline_depth=1
)

# Test configuration 2: Testbench-qualified
conf_2 = Args(
    wgate_continuous=True,
    wstb_continuous=False,
    wen_continuous=True,
    latch_on_strobe=False,
    rstb_continuous=True,
    ren_continuous=False,
    stb_from_en=False,
    wdata_on_wen=False,
    use_mirror=True,
    scalar_pipeline_depth=1
)

_args = conf_2

ACCESS_R=1
ACCESS_W=2
ACCESS_RW=ACCESS_R+ACCESS_W

REGION_SCALAR=0
REGION_ARRAY=1
REGION_MIRROR=2

MEMORY_RANGE_SCALAR = (0,1024)
MEMORY_RANGE_MIRROR = ((1<<23), (1<<24))
MEMORY_RANGE_ARRAY  = (1024, (1<<23))

class MemoryRegion():
    _nregion = 0
    @classmethod
    def _inc(cls):
        cls._nregion += 1
        return

    def __init__(self, addr_range=(0, (1<<24)), label = None):
        self.range = addr_range
        # Each entry is (start, end+1)
        self.map = []
        self.vacant = [list(addr_range)]
        self.refs = []
        if label is None:
            self.label = "MemoryRegion" + str(self._nregion)
            self._inc()
        else:
            self.label = label

    def add(self, width=0, ref=None):
        """Add a memory element of address width 'width' that can optionally
        be referenced by 'ref' (e.g. a variable identifier (pointer))"""
        size = 1<<int(width)
        base = None
        for n in range(len(self.vacant)):
            vmem = self.vacant[n]
            vmem_size = vmem[1]-vmem[0]
            aligned_start = size*math.ceil(vmem[0] / size)
            aligned_size = vmem[1]-aligned_start
            if aligned_size >= size:
                # We can add it!
                # Modify vmem[1] in-place
                old_end = self.vacant[n][1]
                mem_end = aligned_start + size
                if self.vacant[n][0] < aligned_start:
                    # Truncate downward the existing vacant region
                    self.vacant[n][1] = aligned_start
                    if old_end > mem_end:
                        # Add a new vacancy region above the occupied section
                        self.vacant.insert(n+1, [mem_end, old_end])
                else:
                    # Truncate upward the existing vacant region
                    if old_end > mem_end:
                        # Still some vacancy above the occupied section
                        self.vacant[n] = [mem_end, old_end]
                    else:
                        # We occupied the exact size of this vacant region
                        del self.vacant[n]
                # Add the memory region
                self.map.append((aligned_start, mem_end))
                self.refs.append((aligned_start, mem_end, ref))
                base = aligned_start
                break
        if base is None:
            raise Exception("{} has no room for memory of width {}".format(self.label, width))
        return base

    def sort(self):
        self.map.sort(key=lambda x: x[0])
        self.vacant.sort(key=lambda x: x[0])
        self.refs.sort(key=lambda x: x[0])
        return

    def __str__(self):
        ml = len(self.map)
        vl = len(self.vacant)
        mn = 0
        vn = 0
        ss = ["|{:^19s}|{:^19s}|".format("Used", "Free"),
              "|{0}|{0}|".format("-"*19)]
        mempty = ml == 0
        vempty = vl == 0
        while True:
            empty = " "*19
            if mn < ml:
                mem = self.map[mn]
                mstart, mend = mem
            else:
                mempty = True
            if vn < vl:
                vac = self.vacant[vn]
                vstart, vend = vac
            else:
                vempty = True
            if mempty and vempty:
                break
            if vempty or ((not mempty) and (mstart < vstart)):
                memstr = " {:8x}-{:8x} ".format(mstart, mend)
                ss.append("|{}|{}|".format(memstr, empty))
                mn += 1
            else:
                vacstr = " {:8x}-{:8x} ".format(vstart, vend)
                ss.append("|{}|{}|".format(empty, vacstr))
                vn += 1
        return "\n".join(ss)

    def __repr__(self):
        return self.__str__()

    def high_addr(self):
        """Return the highest occupied address + 1."""
        self.sort()
        return self.map[-1][1]

    def base_addr(self):
        """Return the base address of the memory region"""
        return self.range[0]


class Addrspace():
    @staticmethod
    def _vet_ranges(*ranges):
        """Enforce three rules for memory ranges:
            0. range[0] < range[1]
            1. All boundaries must be powers-of-two
            2. Ranges must not overlap
        """
        rl = len(ranges)
        for n in range(rl):
            rng = ranges[n]
            # Rule 0
            low, high = rng
            if low >= high:
                raise Exception("Inverted or identical boundaries: (low, high) = ({}, {})".format(low, high))
                return False
            # Rule 1
            for boundary in rng:
                if (boundary != 0) and (math.log2(boundary) % 1) > 0:
                    raise Exception("Memory address range boundary {} is not a power of two".format(boundary))
                    return False
            # Rule 2
            overlap = False
            if n < rl-1:
                for m in range(n+1, rl):
                    rng_cmp = ranges[m]
                    low_cmp, high_cmp = rng_cmp
                    # Ensure non-overlap
                    # if r0_low < r1_low, then r0_high must also be <= r1_low
                    # else (ro_low > r1_low), then r0_high must also be >= r1_low
                    if (low < low_cmp) != (high <= low_cmp):
                        overlap = True
                    elif (low >= high_cmp) != (high > high_cmp):
                        overlap = True
                    elif (low >= low_cmp) and (high <= high_cmp) :
                        overlap = True
                    elif (low_cmp >= low) and (high_cmp <= high) :
                        overlap = True
                    if overlap:
                        raise Exception("Memory regions (0x{:x}, 0x{:x}) and (0x{:x}, 0x{:x}) overlap".format(
                            low, high, low_cmp, high_cmp))
        return True

    def __init__(self, scalar_range=MEMORY_RANGE_SCALAR, mirror_range=MEMORY_RANGE_MIRROR, array_range=MEMORY_RANGE_ARRAY):
        #self.addr = numpy.empty((0, 2), dtype=int)
        self._vet_ranges(scalar_range, mirror_range, array_range)
        self.scalar_range = scalar_range
        self.mirror_range = mirror_range
        self.array_range = array_range
        self.mem_regions = {
            REGION_SCALAR: MemoryRegion(scalar_range),
            REGION_MIRROR: MemoryRegion(mirror_range),
            REGION_ARRAY:  MemoryRegion(array_range),
        }
        return

    def __str__(self):
        l = []
        for region, mr in self.mem_regions.items():
            if region == REGION_SCALAR:
                rstr = "REGION_SCALAR"
            elif region == REGION_MIRROR:
                rstr = "REGION_MIRROR"
            else:
                rstr = "REGION_ARRAY"
            l.append(rstr)
            l.append(str(mr))
            l.append("")
        return "\n".join(l)

    def __repr__(self):
        return self.__str__()

    def add(self, reg, region=REGION_SCALAR):
        """Add a memory region 'reg' (class Reg) to region 'region'.
        Finds the first empty slot with enough space that is aligned to the
        address width for optimum decoding."""
        return self.mem_regions[region].add(reg.addr_width, ref=reg)

    def sort(self):
        for n in range(len(self.mem_regions)):
            self.mem_regions[n].sort()
        return

    def get_region_mirror_size(self):
        mr = self.mem_regions[REGION_MIRROR]
        return mr.high_addr()-mr.base_addr()

    def get_region_array_size(self):
        mr = self.mem_regions[REGION_ARRAY]
        return mr.high_addr()-mr.base_addr()

    def get_region_scalar_size(self):
        mr = self.mem_regions[REGION_SCALAR]
        return mr.high_addr()-mr.base_addr()

    def conflict(self, base, width):
        # TODO - fix me up
        up = base + (1 << width) - 1
        a1d0 = self.addr[:, 0]
        a1d1 = self.addr[:, 1]
        i1 = numpy.searchsorted(a1d0, base)
        i2 = numpy.searchsorted(a1d1, base)
        i3 = numpy.searchsorted(a1d0, up)
        i4 = numpy.searchsorted(a1d1, up)
        if i1 == i2 and i1 == i3 and i1 == i4:
            if len(self.addr) == 0:
                conflict = False
            else:
                if i1 == len(self.addr):
                    conflict = False
                else:
                    conflict = self.addr[i1, 0] in [up, base] or self.addr[i1, 1] in [up, base]
        else:
            conflict = True
        return conflict

    def diff(self, width=None):
        # TODO wtf is happening here?
        if width == 0 or width is None:
            addr = self.addr
        else:
            addr = self.addr >> width
        return numpy.array([addr[0][0] - 0 if n == 0 else addr[n][0] - addr[n - 1][1] for n in range(addr.shape[0])])

    def nextbase(self, reg):
        if reg.addr_width > 0:
            region = REGION_ARRAY
        elif reg.access == ACCESS_R:
            region = REGION_SCALAR
        else:
            region = REGION_MIRROR
        return self.add(reg, region)

    def valid(self):
        return all(self.diff() > 0)

    def lastaddr(self):
        high_addr = 0
        for region, mr in self.mem_regions.items():
            addr = mr.high_addr()
            if addr > high_addr:
                high_addr = addr
        return high_addr


class regs:
    def __init__(self, jsondict, busdict, avoidjson=[], keep=True, sep=" ", args=_args):
        self.regs = []
        self.busdict = busdict
        self.addrspace = Addrspace()
        for jdictfile in avoidjson:
            with open(jdictfile) as avoidregmapfile:
                avoidjdict = json.load(avoidregmapfile)
            rkeep = regs(avoidjdict, busdict=busdict, keep=True)
            print(self.addrspace)
            self.addrspace.append(rkeep.addrspace)
        for k, v in jsondict.items():
            self.regs.append(Reg(name=k, propdict=v, busdict=busdict, sep=sep, args=args))
        self.assign(keep=keep)
        for r in self.regs:
            r.setbusaddrwidth(self.minbusaddrwidth())
        self.sorted()
        self.args = _args

    def assign(self, keep=True):
        self.sorted()
        if keep:
            # self.assigned=[(r.base_addr,r.base_addr+(1<<r.addr_width)-1) for r in self.regs if r.base_addr!=-1]
            for r in self.regs:
                if r.base_addr != -1:
                    if not self.addrspace.conflict(r.base_addr, r.addr_width):
                        self.addrspace.add(r.base_addr, r.addr_width)
                    else:
                        print("conflicted address, can not keep", r.name, r.base_addr)
                        r.base_addr = -1
            unassigned = [r for r in self.regs if r.base_addr == -1]
        else:
            # self.assigned=[]
            unassigned = self.regs
        #        unusedaddr=[i for i in range(1,len(self.regs)+1) if i not in self.assigned]
        for index, r in enumerate(unassigned):
            # r.base_addr=unusedaddr[index]
            if r.addr_width > 1:
                pass
            r.base_addr = self.addrspace.nextbase(r)
        self.sorted()

    def sorted(self):
        # sregs=sorted(self.regs,key=lambda i:'%8d%8d'%(i.addr_width,i.base_addr))
        sregs = sorted(self.regs, key=lambda i: "%8d%8d" % (i.base_addr, i.addr_width))
        self.regs = sregs

    def addr_comp_v(self, low, high, busname=None):
        """Get Verilog expression of address matching logic for address range [low,high)."""
        w = self.busdict["busaddrwidth"]
        if busname is None:
            busname = self.busdict["busraddrname"]
        l = 0 if low == 0 else int(math.log2(low))
        h = 0 if high == 0 else int(math.log2(high))
        qs = []
        qs.append("{}" + "[{}:{}] == 0".format(w-1, h))
        if l > 0:
            qs.append("|{}" + "[{}:{}]".format(h-1, l))
        qs = ["(" + q.format(busname) + ")" for q in qs]
        qs = " & ".join(qs)
        if (l > 0):
            qs = "(" + qs + ")"
        return qs

    def modportin(self):
        return ",".join([j for j in [i.modportin() for i in self.regs] if j])

    def modportout(self):
        return ",".join([j for j in [i.modportout() for i in self.regs] if j])

    def ro(self):
        return ",".join([i.name for i in self.regs if i.raccess])

    def rw(self):
        return ",".join([i.name for i in self.regs if i.waccess])

    #    def modport(self):
    #        print(self.rw())
    #        return ','.join(['%s,stb_%s'%(i,i) for i in [i.name for i in self.regs if i.waccess]])

    def definestr(self):
        ll = [i.definestr() for i in self.regs]
        if self.args.use_mirror:
            ll.append(self.def_mirror())
        if self.args.scalar_pipeline_depth > 0:
            ll.append(self.def_regbank())
        return "\n\n".join(ll)

    def def_mirror(self):
        """Return Verilog code to instantiate mirror memory with enough capacity for all registers in
        the mirror memory region."""
        mr = self.addrspace.mem_regions[REGION_MIRROR]
        mirror_size = self.addrspace.get_region_mirror_size()
        mirror_aw = math.ceil(math.log2(mirror_size))
        # The full allocated mirror region for decoding
        mirror_aw_allocated = math.ceil(math.log2(mr.range[1]-mr.range[0]))
        # The actual size of the mirror RAM for decoding
        mirror_aw_actual = math.ceil(math.log2(mirror_size))
        mirror_capacity = 1<<mirror_aw
        mirror_base_addr = self.addrspace.mem_regions[REGION_MIRROR].base_addr()
        bus_addr_sel = mirror_base_addr >> mirror_aw_allocated
        ss = [
            "reg [31:0] mirror_ram [0:{}];".format(mirror_capacity-1),
            "reg [31:0] mirror_out_0=0, mirror_in_0=0;",
            "reg [{}-1:0] mirror_addr=0;".format(mirror_aw_actual),
            "reg mirror_rstb=1'b0, mirror_wstb=1'b0;",
            "reg mirror_ren=1'b0, mirror_wen=1'b0;",
            "always @(posedge {busclkname:s}) begin",
            "  mirror_wstb <= mirror_wen & {busstrobename}; // strobe",
            "  mirror_rstb <= mirror_ren & {busstrobename}; // strobe",
            "  mirror_ren<=({buswaddrname:s}[{busaddrwidth:d}-1:" \
            + "{}]=={})".format(mirror_aw_allocated, mirror_base_addr >> mirror_aw_allocated) \
            + "&{busrdenname:s};",
            "  mirror_wen<=({buswaddrname:s}[{busaddrwidth:d}-1:" \
            + "{}]=={})".format(mirror_aw_allocated, mirror_base_addr >> mirror_aw_allocated) \
            + "&{buswrenname:s};",
            "  if (mirror_ren | mirror_wen) begin mirror_addr <= {buswaddrname:s}; end",
            "  if (mirror_wen & {busstrobename}) begin mirror_in_0 <= {buswdataname:s}; end",
            "  if (mirror_wstb) begin mirror_ram[mirror_addr] <= mirror_in_0; end",
            "  if (mirror_rstb) begin mirror_out_0 <= mirror_ram[mirror_addr]; end",
            "end",
        ]
        return "\n".join(ss).format(**self.busdict)

    def def_regbank(self):
        """Return Verilog code to instantiate the reg banks required for the pipelined
        scalar register read decoder.
        Reg banks will be 4-bit decoders, so if we have N registers in this region,
        we'll need ceil(N/16) reg banks."""
        # Recall all registers in this region have aw=0 (no empty addresses)
        nregs = self.addrspace.get_region_scalar_size()
        nbanks = math.ceil(nregs/16)
        _dd = self.busdict.copy()
        raddr = _dd["busraddrname"]
        busraddr_r = (raddr + "_r").replace(".", "_")
        _dd['busraddr_r'] = busraddr_r
        ll = [
            "reg [{busaddrwidth:d}-1:0] {busraddr_r}=0;",
        ]
        for nbank in range(nbanks):
            ll.append("reg [31:0] reg_bank_{}=0;".format(nbank))
        ll.append("wire [{busaddrwidth}-1:0] preseladdr = {busraddrname}[(READDELAY+2)*ADDR_WIDTH-1:(READDELAY+1)*ADDR_WIDTH];")
        # TODO - I could use 'seladdr' instead of creating 'busraddr_r', but that would grab a name from the template
        #        Figure out how to do this in a more portable fashion
        return "\n".join(ll).format(**_dd)

    def readstr_regbank(self):
        """Return Verilog code to instantiate the reg banks required for the pipelined
        scalar register read decoder.
        Reg banks will be 4-bit decoders, so if we have N registers in this region,
        we'll need ceil(N/16) reg banks."""
        # Recall all registers in this region have aw=0 (no empty addresses)
        nregs = self.addrspace.get_region_scalar_size()
        nbanks = math.ceil(nregs/16)
        _dd = self.busdict.copy()
        raddr = _dd["busraddrname"]
        busraddr_r = (raddr + "_r").replace(".", "_")
        _dd['busraddr_r'] = busraddr_r
        low, high = self.addrspace.mem_regions[REGION_SCALAR].range
        _dd['scalar_region_match'] = self.addr_comp_v(low, high, busname=busraddr_r)
        bankh = 0 if high == 0 else int(math.log2(high))-1
        bankaw = bankh-3;
        _dd["bankh"] = bankh
        _dd["bankaw"] = bankaw
        _dd["low_addr"] = low
        _dd["high_addr"] = high-1
        ll = [
        ]
        # TODO - I could use 'seladdr' instead of creating 'busraddr_r', but that would grab a name from the template
        #        Figure out how to do this in a more portable fashion
        ll.append("  {busraddr_r} <= preseladdr;")
        ll.append("  // ===== REGION_SCALAR: 0x{low_addr:x} to 0x{high_addr:x} ====")
        ll.append("  if ({scalar_region_match}) begin  // 0x{low_addr:x} to 0x{high_addr:x}")
        ll.append("    casez ({busraddr_r}[{bankh}:4])")
        for nbank in range(nbanks):
            ll.append("      {0}'h{1:x}: rdata <= reg_bank_{1};".format(bankaw, nbank))
        ll.append("      default: rdata <= 0;")
        ll.append("    endcase")
        ll.append("  end")
        return "\n".join(ll).format(**_dd)

    def mirror_readstr(self):
        mr = self.addrspace.mem_regions[REGION_MIRROR]
        mirror_size = self.addrspace.get_region_mirror_size()
        # The full allocated mirror region for decoding
        mirror_aw_allocated = math.ceil(math.log2(mr.range[1]-mr.range[0]))
        # The actual size of the mirror RAM for decoding
        mirror_aw_actual = math.ceil(math.log2(mirror_size))
        mirror_aw = mirror_aw_allocated
        mirror_base_addr = mr.base_addr()
        mirror_high_addr = mr.range[1]
        bus_addr_sel = mirror_base_addr >> mirror_aw
        busaddrwidth = self.busdict["busaddrwidth"]
        base_addr_selstr="{%d'h%x,{%d{1'b?}}}" \
            % (busaddrwidth - mirror_aw, bus_addr_sel, mirror_aw)
        ss = ["  // ===== REGION_MIRROR: 0x{:x} to 0x{:x} =====".format(mirror_base_addr, mirror_high_addr-1)]
        ss.append("  {}: begin rdata<=mirror_out_0; ".format(base_addr_selstr) \
            + "rvalid<={busrden16name:s}[READDELAY]; rvalidlast<={busrdenlastname:s}[READDELAY]; end".format(**self.busdict))
        return "\n".join(ss)

    def writestr(self):
        strs = [i.writestr() for i in self.regs if i.waccess]
        # Filter out any empty strings
        strs = [ss for ss in filter(lambda x: x != "", strs)]
        return "\n\n".join(strs)

    def readstr(self):
        strs = []
        if self.args.use_mirror:
            strs.append(self.mirror_readstr())
        mr = self.addrspace.mem_regions[REGION_ARRAY]
        array_base_addr, array_high_addr = mr.range
        strs.append("  // ===== REGION_ARRAY: 0x{:x} to 0x{:x} =====".format(array_base_addr, array_high_addr-1))
        strs.extend([i.readstr() for i in self.regs if i.raccess])
        # if self.args.scalar_pipeline_depth:
        #     strs.append(self.readstr_regbank())
        # Filter out any empty strings
        strs = [ss for ss in filter(lambda x: x != "", strs)]
        return "\n".join(strs)

    def readstr_pipelined(self):
        if self.args.scalar_pipeline_depth == 0:
            return ""
        mr = self.addrspace.mem_regions[REGION_SCALAR]
        base = mr.base_addr()
        lastbank = None
        ll = [
            "always @(posedge {busclkname:s}) begin",
            "  if ({busstrobename}) begin",
        ]
        nbank = 0
        for entry in mr.refs:
            addr, end, ref = entry
            nbank = (addr-base) >> 4
            if nbank != lastbank:
                if lastbank is not None:
                    ll.append("      default: reg_bank_{} <= 'habadcafe;".format(nbank))
                    ll.append("    endcase")
                ll.append("    case ({busraddrname:s}[3:0])")
                lastbank = nbank
            sel = (addr-base) & 0xf
            ll.append("      4'h{:x}: reg_bank_{} <= {};".format(sel, nbank, ref.name))
        ll.append("      default: reg_bank_{} <= 'habadcafe;".format(nbank))
        ll.append("    endcase")
        ll.append("  end")
        ll.append("end")
        return "\n".join(ll).format(**self.busdict)

    def readramstr(self):
        strs = [j for j in [i.readramstr() for i in self.regs if i.raccess] if j is not None]
        # Filter out any empty strings
        strs = [ss for ss in filter(lambda x: x != "", strs)]
        return "\n\n".join(strs)

    def jsondict(self):
        return {r.name: r.propdict for r in self.regs}

    def infodict(self):
        dinfo = {k: v for k, v in self.busdict.items()}
        dinfo.update(
            localbusname=clargs.outname,
            rwregs=self.rw(),
            roregs=self.modportout(),
            modport=self.modportin(),
            modportin=self.modportin(),
            modportout=self.modportout(),
            lbdefine=self.definestr(),
            lbwrite=self.writestr() + "\n\n" + self.readramstr(),
            lbread=self.readstr(),
            lbregbank=self.readstr_regbank(),
            lbread_pipelined=self.readstr_pipelined(),
            busaddrwidth=self.minbusaddrwidth(),
        )
        return dinfo

    def minbusaddrwidth(self):
        return int(numpy.ceil(numpy.log2(self.addrspace.lastaddr())))

    def svstr(self):
        return self.busdict["template"] % self.infodict()

    def inputvh(self):
        return "input lbclk,%(modportin)s\n" % self.infodict()

    def updatejson(self, filename):
        self.sorted()
        #        rstrlist=[]
        #        for i,r in enumerate(self.regs):
        #            print(i)
        #            rstrlist.append(r.updatejson())
        with open(filename, "w") as regmapfile:
            regmapfile.write("{\n%s\n}" % ("\n,".join([r.updatejson() for r in self.regs])))


class Reg:
    def __init__(self, name, propdict, busdict, sep=' ', args=_args):
        self.name = name
        self.propdict = {
            "access": "rw",
            "addr_width": 0,
            "data_width": 32,
            "sign": "unsigned",
            "init": "0",
            "base_addr": None,
            "volatile" : False,
        }
        self.propdict.update({k: v for k, v in propdict.items() if k in self.propdict})
        self.access = 0
        # Resolve access type
        if self.waccess:
            self.access += ACCESS_W
        if self.raccess:
            self.access += ACCESS_R
        # Resolve destination memory region
        if self.propdict["addr_width"] > 0:
            self.region = REGION_ARRAY
        elif self.access == ACCESS_R or self.propdict["volatile"]:
            self.region = REGION_SCALAR
        else:
            self.region = REGION_MIRROR

        self.busdict = busdict
        self.busaddrwidth = busdict["busaddrwidth"]
        if self.propdict["addr_width"] > 0:
            print(name, self.propdict["addr_width"])
        self.sep = sep
        self.args = args

    @property
    def addr_width(self):
        return int(eval(str(self.propdict["addr_width"])))

    @property
    def initval(self):
        return int(eval(str(self.propdict["init"])))

    @property
    def sign(self):
        return self.propdict["sign"] == "signed"

    @property
    def base_addr(self):
        base_addr = self.propdict["base_addr"]
        if base_addr is None:
            base_addr = -1
        return base_addr

    @base_addr.setter
    def base_addr(self, value):
        self.propdict["base_addr"] = value

    @property
    def end_addr(self):
        return self.base_addr + (1 << self.addr_width) - 1

    @property
    def waccess(self):
        return "w" in self.propdict["access"]

    @property
    def raccess(self):
        return "r" in self.propdict["access"]

    def setbusaddrwidth(self, value):
        self.busaddrwidth = value

    def infodict(self):
        infodict = {k: v for k, v in self.propdict.items()}
        infodict.update(self.busdict)
        infodict.update(busaddrwidth=self.busaddrwidth)
        infodict.update(name=self.name)
        infodict.update(base_addr=self.base_addr)
        infodict.update(base_addr_sel=self.base_addr >> self.addr_width)
        infodict.update(
            base_addr_selstr="{%d'h%x,{%d{1'b?}}}"
            % (self.busaddrwidth - self.addr_width, self.base_addr >> self.addr_width, self.addr_width)
        )
        infodict.update(sign="signed " if self.sign else "")
        infodict.update(initval=self.initval)

        return infodict

    def definestr(self):
        retstr = ""
        if self.addr_width == 0:
            retstr = """(*dont_touch="yes"*) wire %(sign)s[%(data_width)d-1:0] %(name)s;""" % (self.infodict())
            if self.waccess:
                ss = [
                    "reg %(sign)s[%(data_width)d-1:0] reg_%(name)s=%(initval)d;",
                    "assign %(name)s=reg_%(name)s;",
                ]
                if self.args.wstb_continuous:
                    ss.append("wire wstb_%(name)s=(%(buswaddrname)s==%(base_addr)d)&%(buswrenname)s;  // 0x%(base_addr)x")
                else:
                    ss.append("reg wstb_%(name)s=0;")
                if self.args.wgate_continuous:
                    ss.append("wire wgate_%(name)s=wstb_%(name)s;")
                else:
                    ss.append("reg wgate_%(name)s=0;")
                retstr += self.sep.join(ss) % (self.infodict())
            if self.raccess:
                if self.args.rstb_continuous:
                    retstr += self.sep + ("wire rstb_%(name)s=(%(buswaddrname)s==%(base_addr)d)&%(busrdenname)s&%(busstrobename)s;" \
                           +"  // 0x%(base_addr)x") % (self.infodict())
                else:
                    retstr += self.sep + "reg rstb_%(name)s=0;" % (self.infodict())
        else:
            if self.waccess:
                ss = [
                    "reg [%(addr_width)d-1:0] waddr_%(name)s=0;",
                    "reg %(sign)s[%(data_width)d-1:0] wdata_%(name)s=0;",
                ]
                if self.args.wen_continuous:
                    ss.append("wire wen_%(name)s=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)&%(buswrenname)s;")
                else:
                    ss.append("reg wen_%(name)s=0;")
                if self.args.wstb_continuous:
                    if self.args.stb_from_en:
                        ss.append("wire wstb_%(name)s=wen_%(name)s&%(busstrobename)s;  // 0x%(base_addr)x")
                    else:
                        ss.append("wire wstb_%(name)s=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)" \
                                + "&%(buswrenname)s&%(busstrobename)s;  // 0x%(base_addr)x to " \
                                + "0x{:x}".format(self.end_addr))
                else:
                    ss.append("reg wstb_%(name)s=0;")
                if self.args.wgate_continuous:
                    ss.append("wire wgate_%(name)s=wstb_%(name)s;")
                else:
                    ss.append("reg wgate_%(name)s=0;")
                retstr += self.sep.join(ss) % (self.infodict())
            if self.raccess:
                ss = [
                    "(*dont_touch=\"yes\"*) reg [%(addr_width)d-1:0] raddr_%(name)s=0;",
                    "(*dont_touch=\"yes\"*) wire %(sign)s[%(data_width)d-1:0] rdata_%(name)s;",
                ]
                if self.args.ren_continuous:
                    ss.append("wire ren_%(name)s=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)&%(busrdenname)s;" \
                            + "  // 0x%(base_addr)x to " + "0x{:x}".format(self.end_addr))
                else:
                    ss.append("reg ren_%(name)s=0;")
                if self.args.rstb_continuous:
                    if self.args.stb_from_en:
                        ss.append("wire rstb_%(name)s=ren_%(name)s&%(busstrobename)s;  // 0x%(base_addr)x")
                    else:
                        ss.append("wire rstb_%(name)s=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)" \
                                + "&%(busrdenname)s&%(busstrobename)s;  // 0x%(base_addr)x to " \
                                + "0x{:x}".format(self.end_addr))
                else:
                    ss.append("reg rstb_%(name)s=0;")
                if len(retstr) > 0:
                    retstr += "\n"
                retstr += self.sep.join(ss) % (self.infodict())
        return retstr

    def writestr(self):
        if self.addr_width == 0:
            ss = []
            if not self.args.wgate_continuous:
                ss.append("wgate_%(name)s<=wstb_%(name)s;")
            if not self.args.wstb_continuous:
                ss.append("wstb_%(name)s<=(%(buswaddrname)s==%(base_addr)d)&%(buswrenname)s;")
            if self.args.latch_on_strobe:
                ss.append("if (wstb_%(name)s) reg_%(name)s<=wdata[%(data_width)d-1:0];")
            else:
                ss.append("if ((%(buswaddrname)s==%(base_addr)d)&%(buswrenname)s) reg_%(name)s<=wdata[%(data_width)d-1:0];")
            if len(ss) == 0:
                wstr = ""
            else:
                sep = self.sep
                if sep == "\n":
                    sep = sep + "  " # indent within 'always' block
                wstr = "  " + sep.join(ss) % (self.infodict())
        else:
            ss = []
            if self.args.latch_on_strobe:
                ss.append("if (wstb_%(name)s) begin wdata_%(name)s<=wdata[%(data_width)d-1:0];" \
                        + " waddr_%(name)s<=waddr[%(addr_width)d-1:0]; end")
            elif self.args.wdata_on_wen:
                ss.append("if (wen_%(name)s) begin wdata_%(name)s<=wdata[%(data_width)d-1:0];" \
                        + " waddr_%(name)s<=waddr[%(addr_width)d-1:0]; end")
            else:
                ss.append("if ((%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)&%(buswrenname)s&%(busstrobename)s)" \
                        + " begin wdata_%(name)s<=wdata[%(data_width)d-1:0]; waddr_%(name)s<=waddr[%(addr_width)d-1:0]; end" \
                        + " // 0x%(base_addr)x to " + "0x{:x}".format(self.end_addr))
            if not self.args.wgate_continuous:
                ss.append("wgate_%(name)s<=wstb_%(name)s;")
            if not self.args.wen_continuous:
                ss.append("wen_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)&%(buswrenname)s;" \
                        + " // 0x%(base_addr)x to " + "0x{:x}".format(self.end_addr))
            if not self.args.wstb_continuous:
                if self.args.stb_from_en:
                    ss.append("wstb_%(name)s<=wen_%(name)s&%(busstrobename)s;  // 0x%(base_addr)x to "
                            + "0x{:x}".format(self.end_addr))
                else:
                    ss.append("wstb_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)&%(buswrenname)s" \
                            + "&%(busstrobename)s;  // 0x%(base_addr)x to " + "0x{:x}".format(self.end_addr))
            sep = self.sep
            if sep == "\n":
                sep = sep + "  " # indent within 'always' block
            wstr = "  " + sep.join(ss) % (self.infodict())
        return wstr

    def readramstr(self):
        if self.addr_width == 0:
            if not self.args.rstb_continuous:
                rstr = ("  rstb_%(name)s<=(%(buswaddrname)s==%(base_addr)d)&%(busrdenname)s&%(busstrobename)s;" \
                        + "  // 0x%(base_addr)x to " + "0x{:x}".format(self.end_addr)) % (self.infodict())
            else:
                rstr = ""
        else:
            ss = [
                "if (ren_%(name)s) begin raddr_%(name)s<= waddr[%(addr_width)d-1:0]; end",
            ]
            if not self.args.ren_continuous:
                ss.append("ren_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)" \
                        + "&%(busrdenname)s;  // 0x%(base_addr)x to " + "0x{:x}".format(self.end_addr))
            if not self.args.rstb_continuous:
                if self.args.stb_from_en:
                    ss.append("rstb_%(name)s<=(%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(base_addr_sel)d)" \
                            + "&%(busrdenname)s&%(busstrobename)s;  // 0x%(base_addr)x to " + "0x{:x}".format(self.end_addr))
                else:
                    ss.append("rstb_%(name)s<=ren_%(name)s&%(busstrobename)s;  // 0x%(base_addr)x to " \
                            + "0x{:x}".format(self.end_addr))
            sep = self.sep
            if sep == "\n":
                sep = sep + "  " # indent within 'always' block
            rstr = "  " + sep.join(ss) % (self.infodict())
        return rstr

    def readstr(self):
        # Pipeline'd scalar region reads follow a separate paradigm
        if (self.region == REGION_SCALAR) and (self.args.scalar_pipeline_depth > 0):
            return ""
        else:
            if (self.addr_width == 0):
                # We prevent reads from any entries in the mirror region
                if (self.region == REGION_MIRROR):
                    return ""
                else:
                    ss = [
                        "%(base_addr)d: begin rdata<=%(name)s;",
                        "rvalid<=%(busrden16name)s[READDELAY];",
                        "rvalidlast<=%(busrdenlastname)s[READDELAY];",
                    ]
                    ss.append("end")
            else:
                ss = [
                    "%(base_addr_selstr)s: begin rdata<=rdata_%(name)s;",
                    "rvalid<=%(busrden16name)s[READDELAY];",
                    "rvalidlast<=%(busrdenlastname)s[READDELAY];",
                ]
                ss.append("end")
            rstr = "  " + " ".join(ss) % (self.infodict())
        return rstr

    # w:  wstb, name input
    # rw: wstb,rstb,name  input   if name is written from lb, it should have it there
    # r:  rstb, input name output if name is not written from lb, we have to provide it
    def modportin(self):
        retlist = []
        if self.addr_width == 0:
            if self.waccess:
                retlist.extend([i % self.infodict() for i in ["%(name)s", "wgate_%(name)s", "wstb_%(name)s"]])
            if self.raccess:
                retlist.extend(["rstb_%(name)s" % self.infodict()])
        else:
            if self.waccess:
                retlist.extend(
                    [
                        i % self.infodict()
                        for i in [
                            "waddr_%(name)s",
                            "wdata_%(name)s",
                            "wen_%(name)s",
                            "wgate_%(name)s",
                            "wstb_%(name)s",
                        ]
                    ]
                )
            if self.raccess:
                retlist.extend([i % self.infodict() for i in ["raddr_%(name)s", "ren_%(name)s", "rstb_%(name)s"]])
        return ",".join(retlist)

    def modportout(self):
        #        print('modportout',self.name,self.addr_width)
        retlist = []
        if self.addr_width == 0:
            if self.waccess:
                pass
            elif self.raccess:
                retlist.extend(["%(name)s" % self.infodict()])
        else:
            if self.waccess:
                pass
            if self.raccess:
                retlist.extend(["rdata_%(name)s" % (self.infodict())])
        # print('modportout',retlist)
        return ",".join(retlist)

    def updatejson(self):
        d0 = dict(jsondict=json.dumps(self.propdict, indent=2))
        d0.update(self.infodict())
        return """"%(name)s":%(jsondict)s""" % (d0)


def test_MemoryRegion():
    mr = MemoryRegion()
    widths = [0, 0, 0, 2, 2, 8, 4, 8, 0, 0, 0, 0, 1, 2]
    for w in widths:
        base = mr.add(w)
        print("Adding {} to 0x{:x}".format(w, base))
    mr.sort()
    print(mr)
    return

def _int(s):
    try:
        n = int(s)
        return n
    except ValueError:
        return int(s, 16)


def test__vet_ranges_cli():
    import sys
    import re
    if len(sys.argv) < 3:
        print("Need more than 1 range")
        return
    range_strs = sys.argv[1:]
    ranges = []
    for range_str in range_strs:
        _match = re.match(r"\(([0-9a-fx]+),\s*([0-9a-fx]+)\)", range_str)
        if _match:
            groups = _match.groups()
            low, high = groups
            low = _int(low)
            high = _int(high)
            ranges.append((low, high))
        else:
            print("Failed to match {}".format(range_str))
    print("ranges = {}".format(ranges))
    if len(ranges) < 2:
        print("Need more than 1 range")
        return
    try:
        vet = Addrspace._vet_ranges(*ranges)
        if vet:
            print("Ranges good")
    except Exception as exc:
        print(exc)
    return


def test__vet_ranges():
    # Non power-of-two
    ranges = [(0x101, 0x400), (0x200, 0x400)]
    try:
        vet = Addrspace._vet_ranges(*ranges)
    except Exception as exc:
        print(exc)
        vet = False
    assert not vet

    # high < low
    ranges = [(0x20, 0x10), (0x200, 0x400)]
    try:
        vet = Addrspace._vet_ranges(*ranges)
    except Exception as exc:
        print(exc)
        vet = False
    assert not vet

    # Overlap on high side
    ranges = [(0x100, 0x400), (0x200, 0x400)]
    try:
        vet = Addrspace._vet_ranges(*ranges)
    except Exception as exc:
        print(exc)
        vet = False
    assert not vet

    # Overlap on low side
    ranges = [(0x200, 0x800), (0x100, 0x400)]
    try:
        vet = Addrspace._vet_ranges(*ranges)
    except Exception as exc:
        print(exc)
        vet = False
    assert not vet

    # Overlap on low side
    ranges = [(0x100, 0x1000), (0x200, 0x400)]
    try:
        vet = Addrspace._vet_ranges(*ranges)
    except Exception as exc:
        print(exc)
        vet = False
    assert not vet

    # Overlap on low side (swapped)
    ranges = [(0x200, 0x400), (0x100, 0x1000)]
    try:
        vet = Addrspace._vet_ranges(*ranges)
    except Exception as exc:
        print(exc)
        vet = False
    assert not vet

    # No rules violated
    ranges = [(0x100, 0x200), (0x400, 0x1000)]
    vet = Addrspace._vet_ranges(*ranges)
    assert vet
    print("Nice")
    return


def main():
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", help="regmap.json file", type=str, dest="regmapfile", required=True)
    parser.add_argument("-b", help="localbus.json file", default=None, dest="localbusfile", required=False)
    parser.add_argument(
        "-o", help="regmap generate module and file name", dest="outname", default=None, required=False
    )
    parser.add_argument(
        "-u",
        "--updatejson_file",
        dest="updatejson_file",
        default=None,
        help="Output JSON file with new allocated address",
    )
    parser.add_argument(
        "-k", "--keepaddr", dest="keep", action="store_true", default=False, help="keep existing address"
    )
    parser.add_argument(
        "--avoid", help="other regs to avoid addr conflict", type=str, dest="avoidjson", nargs="*", default=[]
    )
    global clargs # Hack to get clargs.outname to regs.infodict() without passing args through a big stack
    clargs = parser.parse_args()
    print("avoid", clargs.avoidjson)
    # filename='regmap.json'
    with open(clargs.regmapfile) as regmapfile:
        print(clargs.regmapfile)
        regmapdict = json.load(regmapfile)
    if clargs.localbusfile is None:
        localbusdict = {"busaddrwidth": 24}
    else:
        with open(clargs.localbusfile) as localbusfile:
            localbusdict = json.load(localbusfile)
    # TODO - Allow 'sep' to be param as well
    sep = "\n"
    # TODO - Build "args" from clargs
    regmap = regs(regmapdict, busdict=localbusdict, keep=clargs.keep, avoidjson=clargs.avoidjson, sep=sep, args=_args)
    #    print(regmap.svstr())
    header = "// machine-generated by axilb.py\n"
    if clargs.outname is not None:
        f = open(clargs.outname + ".sv", "w")
        f.write(header + regmap.svstr())
        f.close()
        f = open(clargs.outname + "_input.vh", "w")
        f.write(header + regmap.inputvh())
        f.close()
    if clargs.updatejson_file is not None:
        print("Writing new allocated addresses to {}".format(clargs.updatejson_file))
        regmap.updatejson(clargs.updatejson_file)
    #print(regmap.addrspace.addr)
    print(regmap.addrspace)

#    print(regmap.ro())
#    print(regmap.definestr())
#    print(regmap.writestr())
#    print(regmap.readstr())
#    t=reg(name="fclk100",propdict={"access": "r", "addr_width": 0, "data_width": 32, "sign": "unsigned", "init": "0", "base_addr": None})
#    print(t.base_addr)
#    t.base_addr=23
#    print(t.base_addr)

if __name__ == "__main__":
    #test_MemoryRegion()
    #test__vet_ranges()
    main()

