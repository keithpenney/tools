import os


def depd(filename_d, dirin="./", exclude=[]):
    topd = open(filename_d)
    files = []
    for filepath in topd.read().split("\n"):
        if filepath:
            abspath = filepath if os.path.isabs(filepath) else os.path.abspath(dirin + filepath)
            directory = os.path.dirname(dirin + filepath) + "/"
            filename = os.path.basename(dirin + filepath)
            if filename not in exclude:
                filename, ext = os.path.splitext(filename)
                if ext in [".d", ".lib"]:
                    files.extend(depd(abspath, dirin=directory, exclude=exclude))
                else:
                    files.append(abspath)
    return list(set(files))


def extdict(filelist):
    edict = {}
    for f in filelist:
        ext = os.path.splitext(f)[-1]
        if ext not in edict:
            edict[ext] = []
        edict[ext].append(f)
    return edict


def typefile(extdict, typelist):
    olist = []
    for t in typelist:
        if t in extdict:
            olist.extend(extdict[t])
    return olist


def vhpaths(extdict):
    vhpathlist = []
    for f in typefile(extdict, [".vh"]):
        path, base = os.path.split(f)
        if path not in vhpathlist:
            vhpathlist.append(path)
    return vhpathlist


def yvhpaths(filename_d, exclude):
    return " ".join(["-y %s" % i for i in vhpaths(extdict(depd(filename_d=filename_d, exclude=exclude)))])


if __name__ == "__main__":
    #    print('\n'.join(depd(sys.argv[1])))
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument(help="dependent .d file", dest="dfile")
    parser.add_argument(
        "-a",
        "--action",
        help="vsv for .v and .sv, yvh for -y vhpath",
        dest="action",
        type=str,
        choices=["vsv", "yvh"],
        default=None,
    )
    parser.add_argument(
        "-e", "--exclude", help="filelist to be excluded", dest="exclude", type=str, nargs="+", default=[]
    )
    clargs = parser.parse_args()
    if clargs.action is None:
        print("\n".join(depd(filename_d=clargs.dfile, exclude=clargs.exclude)))
    elif clargs.action == "vsv":
        print(
            "\n".join(
                typefile(
                    extdict=extdict(depd(filename_d=clargs.dfile, exclude=clargs.exclude)), typelist=[".sv", ".v"]
                )
            )
        )
    elif clargs.action == "yvh":
        print(yvhpaths(filename_d=clargs.dfile, exclude=clargs.exclude))
    else:
        pass
