template = {
    "all": "{perbus}",
    "perbus": "{Pin} {direction}",
}


def perbus(pindict):
    return "\n".join(sorted([template["perbus"].format(**p) for k, p in pindict.items()]))


def all(pindict):
    return template["all"].format(**dict(perbus=perbus(pindict)))


def writefile(pindict, filename="fpga_master_dir"):
    with open(filename, "w") as f:
        f.write(all(pindict))
