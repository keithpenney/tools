template = {
    "all": "proc packagepinproc {{ }} {{\n {perbus} \nreturn $packagepindict\n}}",
    "perbus": "dict set packagepindict fpga_{pin} {Pin}",
}


def perbus(pindict):
    return "\n".join(sorted([template["perbus"].format(**p) for k, p in pindict.items()]))


def all(pindict):
    return template["all"].format(**(dict(perbus=perbus(pindict), pincount=len(pindict))))


def writefile(pindict, filename="fpga_packagepin.tcl"):
    with open(filename, "w") as f:
        f.write(all(pindict))
