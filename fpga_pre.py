import re
import sys

# sys.path.append("../../../tools/")
import json
import fpga_if_sv
import fpga_inst_vh
import fpga_port_vh
import fpga_via_vh
import fpga_packagepin_tcl
import fpga_pinbank_tcl
import fpga_master_dir

if __name__ == "__main__":
    import fpga_pin

    #    fname="../xc7vx485tffg1761pkg/xc7vx485tffg1761pkg.txt"
    fname = sys.argv[1] + ".txt"
    with open(sys.argv[1] + ".json") as cfgfile:
        cfg = json.load(cfgfile)
    #    validiotype=['HR','HP','GTX']
    #    exceptpin=['M5','M6']
    pins = {
        k: v
        for k, v in fpga_pin.parse_file(fname).items()
        if v["I_O_Type"] in cfg["validiotype"] and k not in cfg["exceptpin"]
    }
    ins = re.compile("|".join(cfg["directioninput"]))
    outs = re.compile("|".join(cfg["directionoutput"]))
    for k, v in pins.items():
        v.update({"pin": v["Pin"].lower()})
        if ins.search(v["Pin_Name"]):
            v.update({"direction": "in"})
            v.update({"dirput": "input"})
        elif outs.search(v["Pin_Name"]):
            v.update({"direction": "out"})
            v.update({"dirput": "output"})
        else:
            v.update({"direction": "inout"})
            v.update({"dirput": "inout"})
    fpga_if_sv.writefile(pins)
    fpga_inst_vh.writefile(pins)
    fpga_port_vh.writefile(pins)
    fpga_via_vh.writefile(pins)
    fpga_packagepin_tcl.writefile({k: v for k, v in pins.items() if v["I_O_Type"] in cfg["packagepin"]})
    fpga_pinbank_tcl.writefile(pins)
    fpga_master_dir.writefile(pins)
