import json
import sys
import re
import os

filename = sys.argv[1]
fpgapackage = os.path.splitext(os.path.basename(filename))[0]
f = open(sys.argv[1])
s = f.read().split("\n")
f.close()
head = s[2]
tail = s[-1]
title = ["Pin", "PinName", "MemoryByteGroup", "Bank", "VCCAUXGroup", "SuperLogicRegion", "IOType", "NoConnect"]
pinsline = r"\s+".join([r"(?P<%s>\S+)" % i for i in title])
pins = {}
for l in s[3:-3]:
    print(pinsline, l)
    m = re.match(pinsline, l)
    if m:
        g = m.groupdict()
        pins[g["Pin"]] = g
    else:
        print("not match", pinsline, l)
if len(sys.argv) >= 2:
    with open(sys.argv[2]) as pininfo:
        pininfo = json.load(pininfo)
        bankiostandard = pininfo["bankiostandard"]
        iopinstype = pininfo["iopinstype"]
else:
    bankiostandard = {
        "14": {"singleended": ["LVCMOS33"], "diff": ["TBD"]},
        "16": {"singleended": ["LVCMOS33"], "diff": ["TBD"]},
        "34": {"singleended": ["LVCMOS33"], "diff": ["TBD"]},
        "35": {"singleended": ["LVCMOS33"], "diff": ["TBD"]},
        "216": {"singleended": [], "diff": []},
    }
    iopinstype = ["HR", "GTP"]
iopins = {k: v for k, v in pins.items() if v["IOType"] in iopinstype}
fpgainterfacestr = """interface %s();
wire %s;
endinterface""" % (
    fpgapackage,
    ",".join(iopins.keys()),
)
print(fpgainterfacestr)
packagepinstr = """set_property PACKAGE_PIN %(Pin)s [get_ports {fpga.%(Pin)s}]"""
iostandardstr = """set_property IOSTANDARD %(IOSTANDARD)s [get_ports {fpga.%(Pin)s}] # %(OTHERIOSTANDARD)s"""
xdcstr = []
for pin, v in pins.items():
    if v["IOType"] in iopinstype:
        xdcstr.append(packagepinstr % v)
        bank = v["Bank"]
        if bank in bankiostandard.keys():
            mdiffpin = re.match(r"IO_\S\d+[PN]_\S+_%s" % v["Bank"], v["PinName"])
            if mdiffpin:
                if len(bankiostandard[bank]["diff"]) > 0:
                    v["IOSTANDARD"] = bankiostandard[bank]["diff"][0]
                    v["OTHERIOSTANDARD"] = str(bankiostandard[bank])
                    xdcstr.append(iostandardstr % v)
            else:
                if len(bankiostandard[bank]["singleended"]) > 0:
                    v["IOSTANDARD"] = bankiostandard[bank]["singleended"][0]
                    v["OTHERIOSTANDARD"] = str(bankiostandard[bank])
                    xdcstr.append(iostandardstr % v)
print("\n".join(xdcstr))
xdcstr.append("""set_property BITSTREAM.General.UnconstrainedPins {Allow} [current_design]""")
f = open("%s.xdc" % fpgapackage, "w")
f.write("\n".join(xdcstr))
f.close()
f = open("%s.vh" % fpgapackage, "w")
f.write(fpgainterfacestr)
f.close()
