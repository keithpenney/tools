import re
import sys


class c_pin:
    def __init__(self, pin, pinname, pin_fw_name=None):
        self.package_pin = pin
        self.pinname = pinname
        self.pin_fw_name = pin_fw_name
        self.direction = "inout"
        self.iostandard = "LVCMOS33"
        self.bank = None
        self.slew = "SLOW"
        self.busname = None
        self.busindex = None

    def xdc(self, usebus=False):
        portname = "%s[%s]" % (self.busname, self.busindex) if usebus else self.pinname
        direction = "set_property direction %s [get_ports {%s}" % (self.direction, portname)
        iostandard = "set_property IOSTANDARD %s [get_ports {%s}]" % (self.iostandard, portname)
        bank = "set_property DRIVE %s [get_ports {%s}]" % (self.bank, portname)
        slew = "set_property SLEW %s [get_ports {%s}]" % (self.slew, portname)
        package_pin = "set_property PACKAGE_PIN %s [get_ports {%s}]" % (self.package_pin, portname)

        return "\n".join([direction, iostandard, bank, slew, package_pin])

    def ucf(self):
        return "NET %s LOC = %s ;" % (self.pinname, self.package_pin)

    def verilog(self):
        return "inout %s" % self.pinname


class ic:
    def __init__(
        self,
        filename,
        pincol,
        pinnamecol,
        pin_fw_namecol,
        startline=None,
        stopline=None,
        mincol=None,
        optcol=None,
        head=None,
        tail=None,
        fw_module=None,
    ):
        self.pins = self.parse_ic_pins(
            filename,
            pincol,
            pinnamecol,
            pin_fw_namecol,
            startline=startline,
            stopline=stopline,
            mincol=mincol,
            optcol=optcol,
            head=head,
            tail=tail,
        )
        self.fw_module = self.parse_verilog(fw_module)

    def parse_ic_pins(
        self,
        filename,
        pincol,
        pinnamecol,
        pin_fw_namecol,
        startline=None,
        stopline=None,
        mincol=None,
        optcol=None,
        head=None,
        tail=None,
        fw_module=None,
    ):
        pins = []
        print(mincol, type(mincol))
        linere = r"\s*".join([r"\s+".join(mincol * [r"(\S+)"]), r"\s*".join(optcol * [r"(\S*)"])])
        s = open(filename).read().split("\n")[startline:stopline]
        for l in s:
            mpin = re.match(linere, l)
            if mpin:
                pin = mpin.group(pincol)
                pinname = mpin.group(pinnamecol)
                pin_fw_name = mpin.group(pin_fw_namecol) if len(mpin.group(pin_fw_namecol)) else None
                pin = c_pin(pin, pinname, pin_fw_name)
                pins.append(pin)
        return pins

    def parse_verilog(self, filename):
        return None


if __name__ == "__main__":
    ictest = ic(sys.argv[1])
    for pin in ictest.pins:
        print(pin.pinname, pin.pin_fw_name)
