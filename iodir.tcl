proc setportdir { portname dir } {
	#	set packagepin [get_property PACKAGE_PIN [get_ports $portname] ]
	set difftype [get_property DIFF_PAIR_TYPE [get_ports $portname] ]
	if { $difftype == "" } {
	} elseif { $difftype == "P" } {
		set diffpairport [get_property DIFF_PAIR_PORT [get_ports $portname] ]
		#		set diffpackagepin [get_property PACKAGE_PIN [get_ports $diffpairport] ]
		set diffpairp $portname
		set diffpairn $diffpairport
		set origpairnet [get_nets -of_objects [get_ports $diffpairport]]
	} elseif { $difftype == "N" } {
		set diffpairport [get_property DIFF_PAIR_PORT [get_ports $portname] ]
		#		set diffpackagepin [get_property PACKAGE_PIN [get_ports $diffpairport] ]
		set diffpairn $portname
		set diffpairp $diffpairport
		set origpairnet [get_nets -of_objects [get_ports $diffpairport]]
	}
	set orignet [get_nets -of_objects [get_ports $portname]]
	disconnect_net -pinlist [get_ports $portname]
	#	set_property DIFF_PAIR_TYPE $difftype [get_ports $portname]
	if { $difftype != "" } {
		puts 1
		set origdiffpairnet [get_nets -of_objects [get_ports $diffpairport]]
		#		split_diff_pair_ports [get_ports $diffpairp] [get_ports $diffpairn]
		puts 2
		disconnect_net -pinlist [get_ports $diffpairport]
		puts 3
		#		set_property PACKAGE_PIN $diffpackagepin [get_ports $diffpairport]
		puts 4
		set_property DIRECTION $dir [get_ports $diffpairport]
		puts 5
		#		set_property PACKAGE_PIN $packagepin [get_ports $portname]
		puts 6
		set_property DIRECTION $dir [get_ports $portname ]
		puts 7
		#		make_diff_pair_ports [get_ports $diffpairp] [get_ports $diffpairn]
		puts 8
		connect_net -hierarchical -object [get_ports $portname] -net $orignet
		connect_net -hierarchical -object [get_ports $diffpairport] -net $origdiffpairnet
	} else {
		#		set_property PACKAGE_PIN $packagepin [get_ports $portname]
		set_property DIRECTION $dir [get_ports $portname ]
		connect_net -hierarchical -object [get_ports $portname] -net $orignet
	}
	puts "$dir: $portname"
}
proc iodir { pinioproc } {
	puts "start iodir $pinioproc"
	set piniodict  [dict create]
	foreach procname $pinioproc {
		if {[info procs $procname] == ""} {
			puts "not proc"
			puts $procname
		} else {
			set iodict [ $procname ]
			puts $iodict
			set modulename [dict get $iodict modulename]
			foreach inpin [dict get $iodict inlist] {
				set inpin_slash [string map { "." "\\\\." } $inpin ]
				set portname [get_ports -of_object [get_nets -of_objects [get_pins -hierarchical -filter "NAME =~ *$modulename/$inpin_slash"] ] ]
				puts "step outside $inpin   $inpin_slash : $portname "
				if { [ string length $portname ] > 0 } {
					puts "$inpin:$portname"
					#			setportdir $portname IN 
					dict set piniodict $portname input
				}
			}
			foreach outpin [dict get $iodict outlist] {
				set outpin_slash [string map { "." "\\\\." } $outpin ]
				set portname [get_ports -of_object [get_nets -of_objects [get_pins -hierarchical -filter "NAME =~ *$modulename/$outpin_slash"] ] ]
				if { [ string length $portname ] > 0 } {
					puts "$outpin,$portname"
					#					setportdir $portname OUT
					dict set piniodict $portname output
				}
			}
		}
	}
	puts $piniodict
	return $piniodict
}


