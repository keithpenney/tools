proc iostandards {pinbankdict bankiostandard} {
	foreach pin [get_ports] {
		if { [dict exist $pinbankdict $pin] } {
			if { [get_property DIFF_PAIR_PORT [get_ports $pin]] eq "" } {
				set pinsediff se
			} else {
				set pinsediff diff
			}
			puts "$pin"
			puts [dict get $pinbankdict $pin]"
			puts [dict get $bankiostandard [dict get $pinbankdict $pin]]"
			set iostandard [dict get [dict get [dict get $bankiostandard [dict get $pinbankdict $pin ] ] ] $pinsediff]
			puts $iostandard
			if {$iostandard eq "none"} {
			} else {
				set_property IOSTANDARD $iostandard [get_ports $pin]
			}
		}
	}
}
