import json


def regmaprw(regmap, busprop):
    readstr = []
    writestr = []
    definestr = []
    write1 = []
    write2 = []
    for name, prop in regmap.items():
        prop.update({"regname": name})
        regcalc = {
            "msbwidth": busprop["busaddrwidth"] - prop["addr_width"],
            "msbval": prop["base_addr"] >> prop["addr_width"],
        }
        if "r" in prop["access"]:
            if prop["addr_width"] == 0:
                readstr.append(
                    """%(busaddrwidth)d'd%(base_addr)d: %(busrdataname)s<= %(regname)s;""" % ({**prop, **busprop})
                )
            else:
                # {14'h1,{10{1'bx}}}: lb.rdata<= bufreadtest.rd.data;
                readstr.append(
                    "{%(msbwidth)d'h%(msbval)d,{%(addr_width)d{1'bx}}}: %(busrdataname)s<= %(regname)s.rd.data;"
                    % ({**prop, **busprop, **regcalc})
                )
        if "w" in prop["access"]:
            prop["init"] = str(prop["init"])
            # writestrold.append('''wire [%(data_width)d-1:0] %(regname)s;reg [%(data_width)d-1:0] reg_%(regname)s=%(init)s;wire stb_%(regname)s;assign stb_%(regname)s=(%(buswaddrname)s==%(base_addr)d)&%(buswritename)s;always @(posedge %(busclkname)s) if(stb_%(regname)s) reg_%(regname)s<=%(buswdataname)s[%(data_width)d-1:0];assign %(regname)s=reg_%(regname)s;'''%({**prop,**busprop}))
            definestr.append(
                """wire [%(data_width)d-1:0] %(regname)s;reg [%(data_width)d-1:0] reg_%(regname)s=%(init)s;wire stb_%(regname)s;assign stb_%(regname)s=(%(buswaddrname)s==%(base_addr)d)&%(buswritename)s;assign %(regname)s=reg_%(regname)s;"""
                % ({**prop, **busprop})
            )
            writestr.append(
                """if(stb_%(regname)s) reg_%(regname)s<=%(buswdataname)s[%(data_width)d-1:0];"""
                % ({**prop, **busprop})
            )
        else:
            if prop["addr_width"] == 0:
                definestr.append("""wire [%(data_width)d-1:0] %(regname)s;""" % ({**prop, **busprop}))
            else:
                definestr.append(
                    """bufread #(.AW(%(addr_width)d),.DW(%(data_width)d)) %(regname)s(.rclk(%(busclkname)s),.ren(%(busreadname)s&%(buswaddrname)s[%(busaddrwidth)d-1:%(addr_width)d]==%(msbwidth)d'h%(msbval)d));
assign %(regname)s.rd.addr=%(buswaddrname)s[%(addr_width)d-1:0];
"""
                    % ({**prop, **busprop, **regcalc})
                )

            write1.append("""wire [%(data_width)d-1:0] %(regname)s;""" % ({**prop, **busprop}))
    print("\n".join(write1))
    print("\n".join(write2))
    print("\n".join(readstr))
    return [definestr, readstr, writestr]


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", help="regmap.json file", type=str, dest="regmapfile")
    parser.add_argument("-b", help="localbus.json file", type=str, dest="localbusfile")
    parser.add_argument("-d", help="lbdefine.vh file", type=str, dest="define", default=None)
    parser.add_argument("-r", help="lbread.vh file", type=str, dest="read", default=None)
    parser.add_argument("-w", help="lbwrite.vh file", type=str, dest="write", default=None)
    clargs = parser.parse_args()
    # filename='regmap.json'
    with open(clargs.regmapfile) as regmapfile:
        regmap = json.load(regmapfile)
    with open(clargs.localbusfile) as localbusfile:
        busprop = json.load(localbusfile)
    [definestr, readstr, writestr] = regmaprw(regmap, busprop)
    if clargs.read:
        with open(clargs.read, "w+") as lbreadfile:
            lbreadfile.write("\n".join(readstr))
    if clargs.write:
        with open(clargs.write, "w+") as lbwritefile:
            lbwritefile.write("\n".join(writestr))
    if clargs.define:
        with open(clargs.define, "w+") as lbdefinefile:
            lbdefinefile.write("\n".join(definestr))

#    f=open('lbread.vh','w+')
#    f.write('\n'.join(readstr))
#    f.close()
#    f=open('lbwrite.vh','w+')
#    f.write('\n'.join(writestr))
#    print("\n".join(readstr))
#    print('\n'.join(writestr))
#    print(clargs)
