import json
import numpy


def regmaprw(regmap, busprop):
    readstr = []
    writestr = []
    definestr = []
    write1 = []
    write2 = []
    inst = []
    for name, prop in regmap.items():
        prop.update({"base_addr": int(str(prop["base_addr"]), 0)})
        prop.update({"regname": name})
        if prop["data_width"] > busprop["busdatawidth"]:
            awr = prop["addr_width"] + int(
                numpy.ceil(round(numpy.log2(prop["data_width"] / busprop["busdatawidth"]), 10))
            )
            dwr = busprop["busdatawidth"]
            print(name, awr)
        else:
            awr = prop["addr_width"]
            dwr = prop["data_width"]

        regcalc = {
            "msbwidth": busprop["busaddrwidth"] - awr,
            "msbval": prop["base_addr"] >> awr,
            "awr": awr,
            "dwr": dwr,
        }
        if "r" in prop["access"]:
            if prop["addr_width"] == 0:
                readstr.append(
                    """%(busaddrwidth)d'd%(base_addr)d: %(busrdataname)s<= %(regname)s;""" % ({**prop, **busprop})
                )
            else:
                # {14'h1,{10{1'bx}}}: lb.rdata<= bufreadtest.rd.data;
                readstr.append(
                    "{%(msbwidth)d'd%(msbval)d,{%(awr)d{1'bx}}}: %(busrdataname)s<= %(regname)s__data;"
                    % ({**prop, **busprop, **regcalc})
                )
        if "w" in prop["access"]:
            prop["init"] = str(prop["init"])
            # writestrold.append('''wire [%(data_width)d-1:0] %(regname)s;reg [%(data_width)d-1:0] reg_%(regname)s=%(init)s;wire stb_%(regname)s;assign stb_%(regname)s=(%(buswaddrname)s==%(base_addr)d)&%(buswritename)s;always @(posedge %(busclkname)s) if(stb_%(regname)s) reg_%(regname)s<=%(buswdataname)s[%(data_width)d-1:0];assign %(regname)s=reg_%(regname)s;'''%({**prop,**busprop}))
            definestr.append(
                """wire [%(data_width)d-1:0] %(regname)s;reg [%(data_width)d-1:0] reg_%(regname)s=%(init)s;wire stb_%(regname)s;assign stb_%(regname)s=(%(buswaddrname)s==%(base_addr)d)&%(buswritename)s;assign %(regname)s=reg_%(regname)s;"""
                % ({**prop, **busprop})
            )
            writestr.append(
                """if(stb_%(regname)s) reg_%(regname)s<=%(buswdataname)s[%(data_width)d-1:0];"""
                % ({**prop, **busprop})
            )
        else:
            if prop["addr_width"] == 0:
                definestr.append("""wire [%(data_width)d-1:0] %(regname)s;""" % ({**prop, **busprop}))
            else:
                definestr.append(
                    """wire [%(dwr)d-1:0] %(regname)s__data;\nwire %(regname)s__en=%(busreadname)s&(%(buswaddrname)s[%(busaddrwidth)d-1:%(awr)d]==%(msbwidth)d'd%(msbval)d);\nwire [%(awr)d-1:0] %(regname)s__addr=%(buswaddrname)s[%(awr)d-1:0];"""
                    % ({**prop, **busprop, **regcalc})
                )
                inst.append(
                    """bufread #(.AWW(%(addr_width)d),.DWW(%(data_width)d),.DWR(%(busdatawidth)d)) %(regname)s(.wclk(),.rclk(),.wdata(),.waddr(0),.wen(),.ren(lbreg.%(regname)s__en),.raddr(lbreg.%(regname)s__addr),.rdata(lbreg.%(regname)s__data),.full(lbreg.%(regname)sfull),.reset(lbreg.stb_%(regname)sreset));"""
                    % ({**prop, **busprop, **regcalc})
                )

            write1.append("""wire [%(data_width)d-1:0] %(regname)s;""" % ({**prop, **busprop}))
    print("\n".join(write1))
    print("\n".join(write2))
    print("\n".join(readstr))
    print("\n".join(inst))
    for k, v in regmap.items():
        v.pop("regname", None)
    return [definestr, readstr, writestr, regmap]


lbtemplate = """interface %(localbusname)s#(parameter LBCWIDTH=8,parameter LBAWIDTH=24,parameter LBDWIDTH=32,parameter WRITECMD=1,parameter READCMD=0)();

ilocalbus #(.LBCWIDTH(LBCWIDTH),.LBAWIDTH(LBAWIDTH),.LBDWIDTH(LBDWIDTH),.READCMD(READCMD),.WRITECMD(WRITECMD))
lb();


reg lbrready_r=0;
reg lbwvalid_r=0;
reg [LBCWIDTH-1:0] lbrcmd_r=0;
reg lbrready_r1=0;
reg lbrready_r2=0;
reg rdbusy=0;
%(lbdefine)s
always @(posedge lb.clk) begin
    %(lbwrite)s
    lbrready_r2<=lbrready_r1;
    rdbusy<=lb.rdbusy;
    case (lb.wctrl)
        lb.writecmd: begin
            lbrready_r1<=lbrready_r;
            lbrready_r<=lb.wvalid;
            lb.rctrl<=lb.wctrl;
            lb.raddr<=lb.waddr;
            lb.rdata<=lb.wdata;
        end
        lb.readcmd: begin
            lbrready_r1<=lbrready_r;
            lbrready_r<=lb.wvalid;
            lb.rctrl<=lb.wctrl;
            lb.raddr<=lb.waddr;
            casex (lb.waddr)
                %(lbread)s
                default: lb.rdata<= 32'hdeadbeef;
            endcase
        end
        default: begin
            lbrready_r<=lb.wvalid;
            lb.rctrl<=lb.wctrl;
            lb.raddr<=lb.waddr;
            lb.rdata<=lb.wdata;
        end
    endcase
end
assign lb.rready= lbrready_r2;
assign lb.wen=1'b1;

`include "%(localbusname)s_modport.vh"
endinterface
"""


def nextaddr(addr, addr_width, maxaddrwidth=24):
    currentmax = max(addr)
    nextbase = ((currentmax >> addr_width) + 1) << addr_width
    nextroof = nextbase + ((1 << addr_width) - 1)
    return nextbase, nextroof


def regmapbaseaddr(regmap, busdatawidth=32):
    addrs = [0]  # numpy.zeros(1<<24,dtype=bool)
    for name in sorted(regmap.keys(), key=lambda item: regmap[item]["addr_width"]):
        aw = regmap[name]["addr_width"] + max(
            0, int(numpy.ceil(round(numpy.log2(regmap[name]["data_width"] / busdatawidth), 10)))
        )
        nextbase, nextroof = nextaddr(addrs, aw)
        addrs.append(nextroof)
        regmap[name]["base_addr"] = nextbase
        print(name, nextbase)
    #    for name in sorted(list(regmap.keys()),key=lambda item : regmap[item['addr_width']]):
    #        print(name,prop['addr_width'])
    return regmap


def writejsoninline(regmap):
    s = []
    for name, prop in regmap.items():
        s.append(""""%s":%s""" % (name, json.dumps(prop)))
    return """{
%s
}""" % (
        "\n,".join(s)
    )


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser()
    parser.add_argument("-m", help="regmap.json file", type=str, dest="regmapfile")
    parser.add_argument("-b", help="localbus.json file", type=str, dest="localbusfile")
    parser.add_argument("-o", help="regmap generate module and file name", type=str, dest="outname", default=None)
    clargs = parser.parse_args()
    # filename='regmap.json'
    with open(clargs.regmapfile) as regmapfile:
        regmap = json.load(regmapfile)
    regmapaddr = regmapbaseaddr(regmap)
    with open(clargs.localbusfile) as localbusfile:
        busprop = json.load(localbusfile)
    [definestr, readstr, writestr, regmapdict] = regmaprw(regmapaddr, busprop)
    if clargs.outname:
        with open(clargs.outname + ".sv", "w+") as svoutfile:
            svoutfile.write(
                lbtemplate
                % (
                    dict(
                        localbusname=clargs.outname,
                        lbdefine="\n".join(definestr),
                        lbwrite="\n".join(writestr),
                        lbread="\n".join(readstr),
                    )
                )
            )
        with open(clargs.outname + ".json", "w+") as jsonoutfile:
            jsonoutfile.write(writejsoninline(regmapdict))
#            json.dump(regmapdict,jsonoutfile,indent=2)

"""
modport cfg(
input test,test2,err,uartmode,xadcupdate,stb_i2cstart,i2cstart,i2cdatatx,clk4ratio,i2cmux_reset_b,fmcdacen,sfptesttx,stb_hwreset,sfptxdisable,mdiodatatx,stb_mdiostart,mdioclk4ratio
,axifmc1adc0_addr,axifmc1adc0_w0r1,axifmc1adc0_wdata,axifmc1adc1_addr,axifmc1adc1_w0r1,axifmc1adc1_wdata,axifmc1dac_addr,axifmc1dac_w0r1,axifmc1dac_wdata,axifmc2adc0_addr,axifmc2adc0_w0r1,axifmc2adc0_wdata,axifmc2adc1_addr,axifmc2adc1_w0r1,axifmc2adc1_wdata,axifmc2dac_addr,axifmc2dac_w0r1,axifmc2dac_wdata,stb_axifmc1adc0_start,stb_axifmc1adc1_start,stb_axifmc1dac_start,stb_axifmc2adc0_start,stb_axifmc2adc1_start,stb_axifmc2dac_start,axifmc1adc0_start,axifmc1adc1_start,axifmc1dac_start,axifmc2adc0_start,axifmc2adc1_start,axifmc2dac_start,si5324_rst,countperrequest,stb_bufreadtestreset
,output xadctemp,xadcaux4,xadcaux12,i2cdatarx,i2crxvalid,fmcprsnt,fmcpgm2c,sfptestrx,hwresetstatus,mdiodatarx,mdiorxvalid,axifmc1adc0_rdata,axifmc1adc0_rdatavalid,axifmc1adc1_rdata,axifmc1adc1_rdatavalid,axifmc1dac_rdata,axifmc1dac_rdatavalid,axifmc2adc0_rdata,axifmc2adc0_rdatavalid,axifmc2adc1_rdata,axifmc2adc1_rdatavalid,axifmc2dac_rdata,axifmc2dac_rdatavalid,macmsb24,maclsb24,ipaddr,bufreadtestfull
,freq_lb,freq_sgmiiclk,freq_sma_mgt_refclk,freq_si5324_out_c,freq_pcie_clk_qo,freq_user_clock,freq_fmc1_llmk_dclkout_2,freq_fmc1_llmk_sclkout_3,freq_fmc1_lmk_dclk8_m2c_to_fpga,freq_fmc1_lmk_dclk10_m2c_to_fpga,freq_fmc2_llmk_dclkout_2,freq_fmc2_llmk_sclkout_3,freq_fmc2_lmk_dclk8_m2c_to_fpga,freq_fmc2_lmk_dclk10_m2c_to_fpga,freq_rxusrclk_sfp,freq_txusrclk_sfp
);
modport dsp(input test,test2,err
,output test1
);"""
