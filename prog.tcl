open_hw_manager
#connect_hw_server -url baci-desktop.dhcp.lbl.gov:3121 -allow_non_jtag
if { [info exists ::env(HWSERVER)] } {
	set hwserver $env(HWSERVER)
} else {
	set hwserver "localhost:3121" 
}
put $hwserver
connect_hw_server -url $hwserver -allow_non_jtag
#-url localhost:3121
set targets [get_hw_target]
set targetslen [llength $targets]
put $targetslen
put [llength $argv]
put $argv
put $targets
set targetindex ""

if { $targetslen == 1 } {
	set targettoload [lindex $targets 0]
} elseif { [ llength $argv ] == 2 } {
	set targettoload [lindex $argv 1]
} else {
	put $targets
	set index 0
	foreach targetinlist $targets {
		puts [ format "%d %s" [lsearch $targets $targetinlist]  $targetinlist ]
	}
	puts "Select which board to program, simple enter to program all"
#	set targetindex [gets  stdin]
	if { [ string length $targetindex ] == 0 } {
		set targettoload $targets
	} else {
		set targettoload [lindex $targets $targetindex]
	}
}
put $targettoload
foreach target $targettoload {
	puts $target
	open_hw_target $target
	set devices [get_hw_devices]
	set deviceindex [lsearch -regexp $devices xc7vx485\s*]
	set device [lindex $devices $deviceindex]
	puts [lindex $argv 0]
	set_property PROGRAM.FILE [lindex $argv 0] [get_hw_devices $device]
	puts "program_hw_device"
	program_hw_devices [get_hw_devices $device]
	close_hw_target $target
}
#close_project
