proc proj {target part outputdir depd} {
puts ${target}
file mkdir ${outputdir}
create_project ${target} ${outputdir} -part ${part}  -force
set decret [depsrc ${depd}]
if [file exist xadcsim] {
	file copy -force xadcsim $outputdir/xadcsim
}
set src [lsort -unique [lindex $decret 0]]
set constrain [lindex $decret 1]
set tcl [lsort -unique [lindex $decret 2]]
set vh [lsort -unique [lindex $decret 3]]
set unk [lsort -unique [lindex $decret 4]]
foreach ip $tcl {
    source $ip
}
#regsub -all "\n" $file_data " " file_data
puts $src
puts $constrain
if { 0 != [llength $src] } {
	add_files $src
}
set_property include_dirs $vh [current_fileset]
if { 0 != [llength $constrain] } {
	add_files -fileset constrs_1 $constrain
}
set_property top $target [current_fileset]
}
