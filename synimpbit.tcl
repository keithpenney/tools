proc synimpbit {outputdir target core {uid ""} {task ""} } {
	puts "uid"
	puts $uid
	if {[current_project -quiet] == ""} {
		open_project ${outputdir}/${target}.xpr
		set_property  top ${target} [current_fileset]
	}
	##set_property STEPS.SYNTH_DESIGN.ARGS.DIRECTIVE RuntimeOptimized [get_runs synth_1]
	##set_property STEPS.OPT_DESIGN.ARGS.DIRECTIVE RuntimeOptimized [get_runs impl_1]
	##set_property verilog_define {MEM_SIZE=24576} [current_fileset]
	##set_property CFGBVS VCCO [current_design]
	##set_property CONFIG_VOLTAGE 1.5 [current_design]
	##set_property STEPS.WRITE_BITSTREAM.TCL.PRE {../../../ignore.tcl} [get_runs impl_1]
	#set_property AUTO_INCREMENTAL_CHECKPOINT 1 [get_runs synth_1]
	#set_property AUTO_INCREMENTAL_CHECKPOINT 1 [get_runs impl_1]
	#set_property write_incremental_synth_checkpoint false [get_runs synth_1]
	#set_param general.maxThreads 1
	if {[info procs packagepinproc] == ""} {
		set packagepindictok false
		puts "no packagepindict"
	} else {
		set packagepindict [packagepinproc]
		set packagepindictok true
		puts "yes packagepindict"
	}

	if {[info procs gtpinproc] == ""} {
		set gtpinlistok false
		puts "no gtpinlist"
	} else {
		set gtpinlist [gtpinproc]
		set gtpinlistok true
		puts "yes gtpinlist"
	}

	if {[info procs pinbankproc] == "" } {
		set pinbankdictok false
		puts "no pinbankdict"
	} else {
		set pinbankdict [pinbankproc]
		set pinbankdictok true
		puts "yes pinbankdict"
	}
	if {[info procs bankiostandardproc] == ""} {
		set bankiostandardok false
		puts "no bankiostandard"
	} else {
		set bankiostandard [bankiostandardproc]
		set bankiostandardok true
		puts "yes bankiostandard"
	}
	global pinioproc
	puts "pinioproc"
	puts $pinioproc
	#	iodir $pinioproc

	if {$task=="UPDATE_FPGA_PORT_VH"} {
		synth_design -rtl -name rtl_1 -mode out_of_context -control_set_opt_threshold 0
		current_design rtl_1
		set piniodict [iodir $pinioproc]
		set piniolist {}
		dict for {k v} $piniodict {
			lappend piniolist "$v $k"
		}
		set piniostr [join $piniolist "\n,"]
		set wfile [open fpga_port_proj.vh "w"]
		puts $wfile $piniostr
		close $wfile
	} else {

		set_property STEPS.SYNTH_DESIGN.ARGS.FLATTEN_HIERARCHY none [get_runs synth_1]
		launch_runs synth_1 -job ${core}
		wait_on_run synth_1
		if { $packagepindictok && $pinbankdictok && $bankiostandardok } {
			open_run synth_1 -name synth_1
			packagepins $packagepindict $gtpinlist
			iostandard $pinbankdict $bankiostandard
			puts "automatic iodir iostandard, and pacagepin"
		}


		set_property SEVERITY {Warning} [get_drc_checks NSTD-1]
		set_property SEVERITY {Warning} [get_drc_checks UCIO-1]
		set_property STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE true [get_runs impl_1]
		#launch_runs impl_1 -to_step write_bitstream -job ${core}
		launch_runs impl_1 to_step route_design -job ${core}
		wait_on_run impl_1
		open_run impl_1
		if { $uid != ""} {
			set_property BITSTREAM.CONFIG.USERID $uid [current_design]
			set_property BITSTREAM.CONFIG.USR_ACCESS NONE [current_design]
		}
		puts "Implementation done!"
		write_bitstream -force ${outputdir}/${target}.runs/impl_1/${target}.bit
		write_debug_probes -force ${outputdir}/${target}.runs/impl_1/${target}.ltx

		write_hw_platform -fixed -include_bit -force -file ${outputdir}/psbd.xsa
		open_run impl_1
		report_timing_summary -delay_type min_max -report_unconstrained -check_timing_verbose -max_paths 10 -input_pins -routable_nets -name timing_1
	}
	close_project
}
