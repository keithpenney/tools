import sys
import re


def veritry(fname):
    modulename = fname.rstrip(".v").split("/")[-1]
    m = re.match(r"([\s\S]*)\.[v|sv]", fname)
    modulename = re.split("/", m.group(1))[-1]
    f = open(fname)
    s = f.read().split("\n")
    f.close()
    io = []
    mio = []
    # plist={}
    mating = {"input": "output", "output": "input", "inout": "inout"}
    for l in s:
        m = re.match(
            r"(module\s+\S+\s*\()?\s*,?(?P<direction>input|inout|output)\s*(?:signed)*(?:reg)*\s*(?P<width>\[[\S\*\+\-]+:[\S\*\+\-]+\])?\s*(?P<name>[a-zA-Z0-9_]*)\s*",
            l,
        )
        #        m=re.match(r'(module\s+\S+\s*\()?\s*,?(?P<direction>input|inout|output)\s*(?:signed)*(?:reg)*\s*(?P<width>\[[\S\*\+\-]+:[\S\*\+\-]+\])?\s*(?P<name>\S*)\s*',l)
        #        m=re.match(r'module\s+(\S+)?\s*\(\s*,?(?P<direction>input|inout|output)\s*(?:signed)*(?:reg)*\s*(?P<width>\[[\S\*\+\-]+:[\S\*\+\-]+\])?\s*(?P<name>\S+)\s*',l)
        if m:
            # p=m.groups()[-1].rstrip(',')
            groupdict = m.groupdict()
            groupdict["width"] = groupdict["width"] or ""
            matingdict = groupdict.copy()
            groupdict["modulename"] = modulename
            matingdict["direction"] = mating[matingdict["direction"]]
            io.append(groupdict)
            mio.append(matingdict)
    # print io,mio
    #    cross='// %s cross wire \n'%(modulename)+ '\n'.join(['wire %(width)s %(name)s;'%(s) for s in sorted(io,key=lambda k:k['name'])])
    #    parent= '// %s parents \n'%(modulename)+ '\n,'.join(['%(direction)s %(width)s %(name)s'%(s) for s in sorted(io,key=lambda k:k['name'])])
    #    mating= '// %s mating \n'%(modulename)+ '\n,'.join(['%(direction)s %(width)s %(name)s'%(s) for s in sorted(mio,key=lambda k:k['name'])])
    print(io)
    sorted(io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()])
    cross = "// %s cross wire \n" % (modulename) + "\n".join(
        [
            "wire %(width)s %(name)s;" % (s)
            for s in sorted(
                io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
            )
        ]
    )
    parent = "// %s parents \n" % (modulename) + "\n,".join(
        [
            "%(direction)s %(width)s %(name)s" % (s)
            for s in sorted(
                io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
            )
        ]
    )
    master = "// %s master \n" % (modulename) + ",".join(
        [
            "%(direction)s %(name)s" % (s)
            for s in sorted(
                io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
            )
        ]
    )
    crossorparent = "// %s cross wire or parent \n" % (modulename) + "\n\n".join(
        [
            "wire %(width)s %(name)s;\n%(direction)s %(width)s %(name)s" % (s)
            for s in sorted(
                io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
            )
        ]
    )
    parentinst = "\n,".join(
        [
            "%(direction)s %(width)s %(modulename)s_%(name)s" % (s)
            for s in sorted(
                io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
            )
        ]
    )
    mating = "// %s mating \n" % (modulename) + "\n,".join(
        [
            "%(direction)s %(width)s %(name)s" % (s)
            for s in sorted(
                io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
            )
        ]
    )
    slave = "// %s slave \n" % (modulename) + ",".join(
        [
            "%(direction)s %(name)s" % (s)
            for s in sorted(
                io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
            )
        ]
    )
    #        plist[p]={}
    #        plist[p]['width']=m.group(2)
    #        o.append('.%s(%s)'%(p,p))
    # print '\n'.join(['wire %s %s;'%(plist[s]['width'],s) if plist[s]['width'] else 'wire %s;'%(s) for s in sorted(plist.keys())])
    inst = "%s %s(%s);" % (
        modulename,
        modulename,
        ",".join(
            [
                ".%(name)s(%(name)s)" % (p)
                for p in sorted(
                    io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
                )
            ]
        ),
    )
    instmod = "%s %s(%s);" % (
        modulename,
        modulename,
        ",".join(
            [
                ".%(name)s(%(modulename)s_%(name)s)" % (p)
                for p in sorted(
                    io, key=lambda l: [s if s.isdigit() else s for s in re.match(r"(\S+?)(\d*)$", l["name"]).groups()]
                )
            ]
        ),
    )
    # print modulename
    # print ','.join(sorted(o)).replace(',)',')').replace(',(','(')
    return [cross, parent, mating, inst, parentinst, instmod, crossorparent, master, slave]


if __name__ == "__main__":
    fname = sys.argv[1]
    [cross, parent, mating, inst, parentinst, instmod, crossorparent, master, slave] = veritry(fname)
    print(cross)
    print(parent)
    print(mating)
    print(crossorparent)
    print(inst)
    print(master)
    print(slave)
