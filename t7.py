import sys
import json

f = open(sys.argv[1])
s = json.load(f)
f.close()
wtemplate = """reg [%(data_width)d-1:0] reg_%(name)s=0; wire stb_%(name)s; assign stb_%(name)s=((lb_adc_addr==%(base_addr)d)&lb_adc_write); always @(posedge lb_clk) if(stb_%(name)s) reg_%(name)s<=lb_adc_dout[%(data_width)d-1:0]; assign %(name)s=reg_%(name)s;"""
rtemplate = """24'd%(base_addr)d: lb_din_r<=%(name)s[%(data_width)d-1:0];"""
wiretemplate = """wire [%(data_width)d-1:0] %(name)s;"""
rlist = []
alist = []
wlist = []
rslave = []
wslave = []
wirelist = []
for name in sorted([k for k in s.keys()], key=lambda x: s[x]["base_addr"]):
    #    print 'base',(s[name]['base_addr'])
    if "w" in s[name]["access"]:
        if int(s[name]["addr_width"]) == 0:
            wlist.append(
                wtemplate
                % ({"data_width": int(s[name]["data_width"]), "name": name, "base_addr": s[name]["base_addr"]})
            )
    if "r" in s[name]["access"]:
        if int(s[name]["addr_width"]) == 0:
            if int(s[name]["data_width"]) <= 32:
                rlist.append(
                    rtemplate
                    % ({"data_width": int(s[name]["data_width"]), "name": name, "base_addr": s[name]["base_addr"]})
                )
            else:
                alist.append(name)
    if s[name]["access"] == "r":
        rslave.append(name)
    else:
        wslave.append(name)
    wirelist.append(wiretemplate % ({"data_width": int(s[name]["data_width"]), "name": name}))
print("\n".join(rlist))
print("\n".join(wlist))
print("\n".join(alist))
print(",".join(["output %s" % r for r in wslave]) + ",".join(["input %s" % r for r in rslave]))
print("\n".join(wirelist))
