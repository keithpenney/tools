source submodules/tools/tclfunc.tcl
set target "gpsdo"
set outputDir ./vivado_project
file mkdir $outputDir
create_project ${target} $outputDir -part "xc7a35tcpg236-2" -force
set decret [depsrc ${target}.d]
set src [lsort -unique [lindex $decret 0]]
set constrain [lsort -unique [lindex $decret 1]]
set tcl [lsort -unique [lindex $decret 2]]
set vh [lsort -unique [lindex $decret 3]]
set unk [lsort -unique [lindex $decret 4]]
foreach ip $tcl {
    source $ip
}
#regsub -all "\n" $file_data " " file_data
puts $src
puts $constrain
add_files $src
add_files $constrain
set_property  top ${target} [current_fileset]

set_property include_dirs $vh [current_fileset]
puts $vh

##set_property STEPS.SYNTH_DESIGN.ARGS.DIRECTIVE RuntimeOptimized [get_runs synth_1]
##set_property STEPS.OPT_DESIGN.ARGS.DIRECTIVE RuntimeOptimized [get_runs impl_1]
##set_property verilog_define {MEM_SIZE=24576} [current_fileset]
##set_property CFGBVS VCCO [current_design]
##set_property CONFIG_VOLTAGE 1.5 [current_design]
##set_property STEPS.WRITE_BITSTREAM.TCL.PRE {../../../ignore.tcl} [get_runs impl_1]

launch_runs synth_1 -job 6
wait_on_run synth_1
set_property SEVERITY {Warning} [get_drc_checks NSTD-1]
set_property SEVERITY {Warning} [get_drc_checks UCIO-1]
launch_runs impl_1 -to_step write_bitstream -job 6
wait_on_run impl_1
puts "Implementation done!"

source submodules/tools/tclfunc.tcl
set outputDir ./vivado_project_sim
set target "gpsdo"
file mkdir $outputDir
create_project $target $outputDir -part "xc7a35tcpg236-2" -force

if [file exist xadcsim] {
	file copy -force xadcsim $outputDir/xadcsim
}
set decret [depsrc ${target}_tb.d]
set src [lsort -unique [lindex $decret 0]]
set constrain [lsort -unique [lindex $decret 1]]
set tcl [lsort -unique [lindex $decret 2]]
set vh [lsort -unique [lindex $decret 3]]
set unk [lsort -unique [lindex $decret 4]]

foreach ip $tcl {
    source $ip
}
#regsub -all "\n" $file_data " " file_data
puts $src
puts $constrain
add_files $src
#add_files $constrain
#add_files -norecurse {/home/ghuang/work/SLAC/lcls2llrf/lcls2_llrf/firmware/submodules/peripheral-drivers/ad9653_sim.v /home/ghuang/work/SLAC/lcls2llrf/lcls2_llrf/firmware/submodules/peripheral-drivers/ad7794_sim.v /home/ghuang/work/SLAC/lcls2llrf/lcls2_llrf/firmware/submodules/peripheral-drivers/amc7823_sim.v}
#add_files -norecurse /home/ghuang/work/SLAC/lcls2llrf/lcls2_llrf/firmware/submodules/common-hdl/oversampled_rx_8b9b.v
update_compile_order -fileset sources_1

#open_project vivado_project/prc.xpr
set_property -name {xsim.simulate.runtime} -value {000ns} -objects [current_fileset -simset]
set_property include_dirs $vh [current_fileset]
set_property top ${target}_tb [get_filesets sim_1]
set_property top_lib xil_defaultlib [get_filesets sim_1]
#set_property top ${target}_tb [current_fileset]

update_compile_order -fileset sim_1
#puts [pwd]
launch_simulation
#run 3000ns
open_vcd
log_vcd
puts [lindex $argv 0]
run [lindex $argv 0]
#1000ns
close_vcd
close_sim
